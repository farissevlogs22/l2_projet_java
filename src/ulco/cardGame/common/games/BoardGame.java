package ulco.cardGame.common.games;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.util.List;

public abstract class  BoardGame implements Game {
    /**
     * Attributs protected
     */
    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started =false;

    /**
     * Les méthodes de la classeBoardGame
     */

    /*
    Constructeur de la classe
     */
    public BoardGame(String name,Integer maxPlayers,String filename){
        this.name = name;
        this.maxPlayers =maxPlayers;
        initialize(filename);
    }

    /*
    Methode permettant d'ajouter des joueurs
     */

    public boolean addPlayer(Player player) {
        if (maxPlayers == maxNumberOfPlayers()) this.started = true;


        else {
            for (Player me : this.getPlayers()) {
                if (player.getName() == me.getName()) {
                    me.canPlay(false);
                    return false;
                }

            }
            this.players.add(player);
            this.started = false;

        }
        return true;
    }




    /*
    Méthode permettant de supprimer les joueurs
    */
    public void removePlayer(Player player) {
        for (Player me : this.getPlayers()) {
            if (me.getName() == player.getName()) {
                this.players.remove(me);
            }
        }
    }
/*
Méthode permettant de supprimer tous les joueurs du jeu
 */

    public void removePlayers(){
        this.players.clear();
    }

    /*
    Méthode permettant d'afficher l'état du jeu
     */

    @Override
    public void displayState() {
        for(Player tous: this.getPlayers()){
            System.out.printf("Name: %s, stam : %d",tous.getName(),tous.getScore());
        }
    }

    /*
    Méthode permettant de savoir si le jeu a commencé
     */
    public boolean isStarted() {
        return this.started;
    }

/*
Méthode permettant de retourner le nombre maximum de joueurs
 */

    public Integer maxNumberOfPlayers(){
        return this.maxPlayers;
    }

    /*
    Méthode permettant de récupérer la liste des joueurs du jeu en cours
     */
    public List<Player>getPlayers(){
        return this.players;
    }
}
